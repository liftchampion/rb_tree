#include <stdio.h>

# include <stdlib.h>
#include "libft.h"
#include "ft_debugger.h"
#include "ft_rb_tree_visualizator.h"

#include <zconf.h>


# define GREY "\x1B[0m\x1B[37m"




#include "ft_map.h"


int ft_comparator(int a, int b)
{
	return (a - b);
}

void ft_big_test(t_rb_tree **tree, t_map *map);
void ft_test_tree(t_rb_tree *tree, t_color prev_color, int count_bh, int bh_et, t_rb_tree *nil);
void ft_find_rb_bh(t_rb_tree *tree, int *bh, t_rb_tree *nil);

#include <time.h>
#include "ft_types.h"

int main() {
	t_rb_tree *tree = 0;

	t_int8 uuu = 0;



	int y = 3;
	int z = 4;
	int x = 2;
	int z1 = 8;
	int z2 = 1;
	int z3 = 7;
	int z4 = 2;

	t_map *map = ft_make_std_map(STRING, STRING);

	return 0;
}


void ft_test_tree(t_rb_tree *tree, t_color prev_color, int count_bh, int bh_et, t_rb_tree *nil)
{
	if (tree == nil || !tree)
	{
		if (count_bh != bh_et)
		{
			printf(RED "\n BH ERROR!!!");
		}
		//printf(GREEN "\n BRANCH FINISHED");
		return ;
	}
	if (prev_color == RB_RED && tree->color == RB_RED)
	{
		printf(RED "\n RED - RED ERROR!!!");
	}
	if (tree->color == RB_BLACK)
		count_bh++;
	if (tree->left != nil && tree->right != nil)
	{
		if (tree->left->key >= tree->right->key)
			printf(RED "\n SORTING ERROR!!!");
		if (tree->left->key >= tree->key)
			printf(RED "\n SORTING ERROR!!!");
		if (tree->right->key <= tree->key)
			printf(RED "\n SORTING ERROR!!!");
	}
	ft_test_tree(tree->left, tree->color, count_bh, bh_et, nil);
	ft_test_tree(tree->right, tree->color, count_bh, bh_et, nil);
}

void ft_find_rb_bh(t_rb_tree *tree, int *bh, t_rb_tree *nil)
{
	if (tree == nil || !tree)
	{
		return ;
	}

	if (tree->color == RB_BLACK)
		(*bh)++;
	ft_find_rb_bh(tree->left, bh, nil);
}


#include <time.h>

void ft_check_in(t_rb_tree *tree, int val, int *found, t_rb_tree *nil)
{
	if (tree == 0)
	{
		printf(BLUE "PUSTOE!!! AHUET'!!!\n");
		return ;
	}
	if (tree == nil)
	{
		return ;
	}
	if (val == (int)tree->key)
	{
		(*found)++;
	}
	if (val < (int)tree->key)
	{
		ft_check_in(tree->left, val, found, nil);
	}
	if (val > (int)tree->key)
	{
		ft_check_in(tree->right, val, found, nil);
	}
}

void ft_count(t_rb_tree *tree, int *count, t_rb_tree *nil)
{
	if (tree == 0)
	{
		sleep(2);
		printf(BLUE "PUSTOE!!! AHUET'!!!\n");
		return ;
	}
	if (tree == nil)
	{
		return ;
	}
	(*count)++;
	ft_count(tree->left, count, nil);
	ft_count(tree->right, count, nil);
}


#include <math.h>

void ft_big_test(t_rb_tree **tree, t_map *map)
{

	//t_rb_tree *tree = 0;
	int count_ops = 1000;
	int r;
	srand(time(NULL));
	int bh = 0;


	int ins1[] = {13, 20, 21, 22, 16, 24, 0, 6, 2, 14};
	int del1[] = {1, 15, 17, 19, 4, 24, 2, 11, 4, 2};

	///4, 16, 19, 7, 13, 3, 10, 11, 0, 23, 14, 5, 21
	///16, 0, 10, 3, 13, 23

	int ins2[] = {4, 16, 19, 7, 13, 3, 10, 11, 0, 23, 13, 14, 5, 16, 21};
	int del2[] = {8, 16, 0, 10, 6, 24, 22, 3, 20, 13, 23, 1, 1, 21, 11};

	//int ins[count_ops];
	//int del[count_ops];

	///(1,63)(2,228)(3,396)(4,566)(5,742)(6,1008)(7,1286)(8,1802)(9,1944)
	/// (10,2085)(11,2573)(12,2997)(13,3544)(14,4134)(15,4803)

	double fTimeStart;
	double fTimeStop;

	double total_time = 0;

	int found;

	for (int e = 0; e < count_ops; e++)
	{
		r = rand() % 7000;
		//r = ins2[e];
		///fTimeStart = clock()/(double)CLOCKS_PER_SEC;
		rb_tree_insert_data(map, (void*)(size_t)r);
		///fTimeStop = clock()/(double)CLOCKS_PER_SEC;
		///total_time += fTimeStop - fTimeStart;
		bh = 0;
		found = 0;
		ft_check_in(*tree, r, &found, map->nil);
		ft_find_rb_bh(*tree, &bh, map->nil);
		if (found != 1)
		{
			printf(RED "SUKABLYAT!!!!!!!! ins %d %d\n", found, r);
			ft_PIZDATO(*tree, map->nil);
			return ;
		}

		/////////ft_test_tree(*tree, RB_BLACK, 0, bh, map->nil);
		///if (e % (count_ops / 100) == 0)
		///{
			//fTimeStop = clock()/(double)CLOCKS_PER_SEC;
			///printf(GREEN "%d ITERS FINIFHED   time spent %f ms\n", (count_ops / 100), ((fTimeStop - fTimeStart) / (count_ops / 100) * 1000));
			//fTimeStart = clock()/(double)CLOCKS_PER_SEC;
		///}
	}
	///printf(GREEN "INSERT TESTS FINIFHED\n");
	//ft_PIZDATO(tree);
	///printf("\n");

	//fTimeStop = clock()/(double)CLOCKS_PER_SEC;
	///printf("all inserts: %f per_op: %f mks\n", total_time, ((total_time) / count_ops) * 1000 * 1000);
	total_time = 0;

	for (int e = 0; e < count_ops; e++)
	{
		r = rand() % 7000;
		//r = del2[e];
		///fTimeStart = clock()/(double)CLOCKS_PER_SEC;
		rb_tree_delete_key(map, (void*)(size_t)r);
		///fTimeStop = clock()/(double)CLOCKS_PER_SEC;
		///total_time += fTimeStop - fTimeStart;
		bh = 0;
		found = 0;
		ft_find_rb_bh(*tree, &bh, map->nil);
		ft_check_in(*tree, r, &found, map->nil);
		if (found != 0)
			printf(RED "SUKABLYAT!!!!!!!! del %d\n", found);
		///////////ft_test_tree(*tree, RB_BLACK, 0, bh, map->nil);
		///if (e % (count_ops / 100) == 0)
		///{
			//fTimeStop = clock()/(double)CLOCKS_PER_SEC;
			///printf(GREEN "%d ITERS FINIFHED   time spent %f ms\n", (count_ops / 100), ((fTimeStop - fTimeStart) / (count_ops / 100) * 1000));
			//fTimeStart = clock()/(double)CLOCKS_PER_SEC;
		///}
		//printf("after del %d\n", r);
		//ft_PIZDATO(tree);
		//printf("\n");
	}
	///printf(GREEN "DEL TESTS FINIFHED\n");
	///printf("all dels: %f per_op: %f mks\n", total_time, ((total_time) / count_ops) * 1000 * 1000);
	///printf("\n");
	//for (int e = 0; e < count_ops; e++)
	//	printf("%d ", ins[e]);
	//printf("\n");
	//for (int e = 0; e < count_ops; e++)
	//	printf("%d, ", del[e]);



	int count = 0;
	ft_count(*tree, &count, map->nil);
	int levels = rb_tree_level_count(*tree, map->nil);
	double kaef = 2 * log2(count + 1);
	//printf(RED "\n deep:%d nodes:%d kaef:%f", levels, count, kaef);
	///printf(BLUE "%d %d %f\n", levels, count, kaef);
	if (levels > kaef)
		printf(RED "TI AHUEL IDI NAHUI PIDOR SUKABLYAT %d %f\n", levels, kaef);


	//ft_PIZDATO(tree);

	//printf(RED "\n %d  %d", rb_tree_find_min(tree)->key, rb_tree_find_max(tree)->key);
}


