all: main.c libft.a
	clang -c *.c
	clang *.o libft.a -o main
	./main
valgrind: all
	valgrind --track-origins=yes --leak-check=full --show-leak-kinds=definite --errors-for-leak-kinds=definite ./main